
# [Question2Answers][q2a] v1.8.3 with [s2i] file

This is a copy to easily deploy to Openshift [using source to image][s2i]

## Openshift deployment

    $ oc new-app --name q2a-app -l app=q2a,service=q2a-app centos/php-72-centos7~https://gitlab.cee.redhat.com/docker-images/q2a.git#v1.8.3
    $ oc set env bc/q2a-app GIT_SSL_NO_VERIFY=1
    $ cat .env
    MYSQL_DATABASE=q2a
    MYSQL_PASSWORD=password
    MYSQL_ROOT_PASSWORD=password
    MYSQL_USER=q2a
    MYSQL_HOST=q2a-db
    $ oc delete configmap/q2a-app-configmap || :
    $ oc create configmap q2a-app-configmap --from-env-file=.env
    $ oc set env dc/q2a-app --from configmap/q2a-app-configmap
    $ oc start-build bc/q2a-app
    $ oc logs -f dc/q2a-app
    $ oc create route edge q2a-app --service q2a-app --hostname=q2a.cloud.paas.lab.upshift.redhat.com

[q2a]: https://github.com/q2a/question2answer/releases/tag/v1.8.3
[s2i]: https://github.com/openshift/source-to-image
